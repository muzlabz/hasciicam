## HASCIICAM 

## Developer maintainer Jaromil :: Dynabolic :: 

#### Dynabolic
- https://www.dyne.org/software/hasciicam/

#### Github jaromil
- https://github.com/jaromil/hasciicam

#### Arch wiki hasciicam
- https://aur.archlinux.org/packages/hasciicam

#### Linux install issue with solution
- https://github.com/jaromil/hasciicam/issues/13 

---

### Live session 400x500
- hasciicam -s 400 500   

#### examples html fonts
- hasciicam -a bitsream  -S 2 -m html -o hasciicam.html
- hasciicam -a verdana -S 2 -m html -o hasciicam.html 
- hasciicam -a arial  -S 3 -m html -o hasciicam.html
- hasciicam -a noto -S 2 -m html -o hasciicam.html 
- hasciicam -a nimbus -S 3 -m html -o hasciicam.html 
- hasciicam -a ubuntu -S 2 -m html -o hasciicam.html 

#### ARCH dependencies >>>> logout / login
- xorg-font-util

- xorg-fonts-100dpi

- xorg-fonts-75dpi

- xorg-fonts-alias

- xorg-fonts-encodings

- xorg-fonts-misc

- xorg-fonts-type1

- xorg-mkfontscale

- xorg-xlsfonts

---

